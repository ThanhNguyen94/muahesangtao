package thanh.apprunning;

import java.io.Serializable;

import android.graphics.drawable.Drawable;

public class App2 implements Serializable{
	
	String nameApp;
	Drawable iconApp;
	String packageApp;
	
	public String getPackageApp() {
		return packageApp;
	}
	public void setPackageApp(String packageApp) {
		this.packageApp = packageApp;
	}
	public App2()
	{
		
	}
	public String getNameApp() {
		return nameApp;
	}
	public void setNameApp(String nameApp) {
		this.nameApp = nameApp;
	}
	public Drawable getIconApp() {
		return iconApp;
	}
	public void setIconApp(Drawable iconApp) {
		this.iconApp = iconApp;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nameApp + "xx"+iconApp;
	}
	
}
