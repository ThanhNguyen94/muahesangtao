package thanh.apprunning;

import java.util.ArrayList;



import mhst.iuh.batterysaverdemo.*;
import android.R.array;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AppRunAdapter extends ArrayAdapter<App2>{
	ArrayList<App2> arrayApp;
	Context contexr;
	public AppRunAdapter(Context context, int resource, ArrayList<App2> arrApp) {
		super(context, R.layout.row, arrApp);
		// TODO Auto-generated constructor stub
		this.contexr = context;
		this.arrayApp = arrApp;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater inf=(LayoutInflater)
				getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View rowview=inf.inflate(R.layout.row ,parent, false);
		TextView tv = (TextView)rowview.findViewById(R.id.apptitle);
		ImageView img = (ImageView)rowview.findViewById(R.id.appicon);
		tv.setText(arrayApp.get(position).getNameApp());
		img.setImageDrawable(arrayApp.get(position).getIconApp());
		
		
		return rowview;
	}
}
