package andoid.graphview;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;


public class GraphDraw {
	public Intent getIntent(Context context) {

		int[] x = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}; // x values!
		int[] y =  { 100, 60, 10, 50, 40, 8, 56, 80 ,100 ,1, 10, 34, 56, 12, 34, 89, 45, 67, 23, 45, 78,56, 23, 89 }; // y values!
		TimeSeries series = new TimeSeries("Level Battery"); 
		for( int i = 0; i < x.length; i++)
		{
			series.add(x[i], y[i]);
		}
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		dataset.addSeries(series);
		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer(); // Holds a collection of XYSeriesRenderer and customizes the graph
		XYSeriesRenderer renderer = new XYSeriesRenderer(); // This will be used to customize line 1
		mRenderer.addSeriesRenderer(renderer);
		// Customization time for line 1!
		renderer.setColor(Color.RED);
		renderer.setLineWidth(3);
		renderer.setPointStyle(PointStyle.SQUARE);
		renderer.setFillPoints(true);
		Intent intent = ChartFactory.getLineChartIntent(context, dataset, mRenderer, "Line Graph Title");
		return intent;
	}

}
