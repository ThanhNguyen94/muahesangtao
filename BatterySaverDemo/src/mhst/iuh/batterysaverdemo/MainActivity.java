package mhst.iuh.batterysaverdemo;


import java.util.ArrayList;
import java.util.List;

import thanh.apprunning.ListInstalledApps;
import andoid.graphview.GraphDraw;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;



public class MainActivity extends Activity implements OnClickListener{

	ImageView imgbatteryinfo;
	TextView tvbaterryinfo;
	TextView tvlevelpin, tchar;
	ArrayList<String >apprun;
	
	Button btlevel, btvoltage, bttech, btoptimize;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		
		this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	}

	//intit app
	public void init()
	{
		imgbatteryinfo = (ImageView)findViewById(R.id.imgbateryinfo);
		tvbaterryinfo = (TextView) findViewById(R.id.tvbatteryinfo);
		tvlevelpin = (TextView) findViewById(R.id.tvlevevlpin);
		btlevel = (Button) findViewById(R.id.btlevel);
		btvoltage = (Button) findViewById(R.id.btvoltage);
		bttech = (Button) findViewById(R.id.bttech);
		tchar = (TextView)findViewById(R.id.tvchar);
		apprun = new ArrayList<String>();
		btoptimize = (Button)findViewById(R.id.btoptimte);
		bttech.setOnClickListener(this);
		btlevel.setOnClickListener(this);
		btvoltage.setOnClickListener(this);
		btoptimize.setOnClickListener(this);
		tchar.setOnClickListener(this);
	}
	

	//get battery information 
	private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {

		@Override

		public void onReceive(Context context, Intent intent) {

			int  health= intent.getIntExtra(BatteryManager.EXTRA_HEALTH,0);
			int  icon_small= intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL,0);
			int  level= intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
			int  plugged= intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
			boolean  present= intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT); 
			int  scale= intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
			int  status= intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
			String  technology= intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
			int  temperature= intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
			int  voltage= intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);
			tvlevelpin.setText(""+level+"%");

			tvbaterryinfo.setText("Time left");
			btlevel.setText("Temperature\n "+temperature/10+"*C");
			bttech.setText("Technology\n"+technology);
			btvoltage.setText("Voltage\n"+voltage +"V");

		}

	};
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btlevel:
			Intent i = new Intent(MainActivity.this, BatteryInfo.class);
			startActivity(i);
			break;

		case R.id.btvoltage:
			Intent i1 = new Intent(MainActivity.this, BatteryInfo.class);
			startActivity(i1);
			break;

		case R.id.bttech:
			Intent i2 = new Intent(MainActivity.this, BatteryInfo.class);
			startActivity(i2);
			break;

		case R.id.btoptimte:
			Intent i3 = new Intent(MainActivity.this, Optimize.class);
			startActivity(i3);
			break;
		
		case R.id.tvchar:
			GraphDraw line = new GraphDraw();
	    	Intent lineIntent = line.getIntent(this);
	        startActivity(lineIntent);
		default:
			break;
		}

	}


}
