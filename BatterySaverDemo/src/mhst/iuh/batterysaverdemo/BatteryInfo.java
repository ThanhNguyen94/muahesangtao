package mhst.iuh.batterysaverdemo;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BatteryInfo extends Activity {
	double batteryCapacity;
	ListView lv1;
	ArrayAdapter<String>adt;
	ArrayList<String> arr1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.batteryinfo);
		setinit();
		getBatteryCapacity();
		this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		adt = new ArrayAdapter<String>(BatteryInfo.this, android.R.layout.simple_list_item_1, arr1);
		lv1.setAdapter(adt);	
		
	}
	
	
	
	public void setinit()
	{
		lv1 = (ListView)findViewById(R.id.listView1);
		arr1 = new ArrayList<String>();
		
	}
	
	
	
	
	
	//get battery information 
		private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {

			@Override

			public void onReceive(Context context, Intent intent) {

				int  health= intent.getIntExtra(BatteryManager.EXTRA_HEALTH,0);
				int  icon_small= intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL,0);
				int  level= intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
				int  plugged= intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
				boolean  present= intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT); 
				int  scale= intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
				int  status= intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
				String  technology= intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
				int  temperature= intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
				int  voltage= intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);
				arr1.add("temperarure : "+temperature+"*C");
				arr1.add("Technology: " + technology);
				arr1.add("Voltage : "+voltage+" V");	
				arr1.add("Capacity : "+batteryCapacity);
				arr1.add("Health : "+health);
				arr1.add("Level : "+level);
				arr1.add("Status : "+status);
				
			}

		};
	
	

	public void getBatteryCapacity() {
		Object mPowerProfile_ = null;

		final String POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile";

		try {
			mPowerProfile_ = Class.forName(POWER_PROFILE_CLASS)
					.getConstructor(Context.class).newInstance(this);
		} catch (Exception e) {
			e.printStackTrace();
		} 

		try {
			batteryCapacity = (Double) Class
					.forName(POWER_PROFILE_CLASS)
					.getMethod("getAveragePower", java.lang.String.class)
					.invoke(mPowerProfile_, "battery.capacity");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
