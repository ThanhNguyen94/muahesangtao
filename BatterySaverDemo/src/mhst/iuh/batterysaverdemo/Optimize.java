package mhst.iuh.batterysaverdemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import thanh.apprunning.App2;
import thanh.apprunning.AppRunAdapter;
import mhst.iuh.batterysaverdemo.R.drawable;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class Optimize extends Activity {
	
	ListView  lvapprunning;
	ArrayAdapter<App2> adt;
	ActivityManager activityManager;
	ArrayList<App2> arrApp;
	App2 app;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.optimize);
		arrApp = new ArrayList<App2>();
		lvapprunning = (ListView) findViewById(R.id.listView1);

		//		Context context = this.getApplicationContext();
		//		ActivityManager mgr = (ActivityManager)context.getSystemService(ACTIVITY_SERVICE);
		//		List<RunningAppProcessInfo> processes = mgr.getRunningAppProcesses();
		//		Log.e("DEBUG", "Running processes:");
		//		for(Iterator i = processes.iterator(); i.hasNext(); )
		//		{
		//		RunningAppProcessInfo p = (RunningAppProcessInfo)i.next();
		//		Log.e("DEBUG", "  process name: "+p.processName);
		//		apps.add(p.processName);
		//		Log.e("DEBUG", "     pid: "+p.pid);                    // process id
		//		switch (p.importance) {
		//		case ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND: Log.e("DEBUG","       is IMPORTANCE_FOREGROUND"); break;
		//		case ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE: Log.e("DEBUG","       is IMPORTANCE_VISIBLE"); break;
		//		case ActivityManager.RunningAppProcessInfo.IMPORTANCE_SERVICE: Log.e("DEBUG","       is IMPORTANCE_SERVICE"); break;
		//		case ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND: Log.e("DEBUG","       is IMPORTANCE_BACKGROUND"); break;
		//		case ActivityManager.RunningAppProcessInfo.IMPORTANCE_EMPTY: Log.e("DEBUG","       is IMPORTANCE_EMPTY"); break;
		//		default: Log.e("DEBUG","       should not happend"); break;
		//		            }
		//		}
		//		Log.e("debug","killing com.android.browser");
		//		mgr.killBackgroundProcesses("com.android.browser");
		//		Log.e("debug","processes again");
		//		mgr.killBackgroundProcesses("mhst.iuh.batterysaverdemo");
		//		Log.e("debug","killing this ap");
		//		for(Iterator i = processes.iterator(); i.hasNext(); )
		//		{
		//		RunningAppProcessInfo p = (RunningAppProcessInfo)i.next();
		//		Log.e("DEBUG", "  process name: "+p.processName);
		//		Log.e("DEBUG", "     pid: "+p.pid);                    // process id
		//		}
		getApprun();
		
		adt = new AppRunAdapter(Optimize.this, R.layout.row, arrApp);
		lvapprunning.setAdapter(adt);
		lvapprunning.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				int pid = android.os.Process.getUidForName(arrApp.get(position).getPackageApp());
				android.os.Process.killProcess(pid);
				activityManager.killBackgroundProcesses(arrApp.get(position).getPackageApp().toString());
				Toast.makeText(Optimize.this,"Đã Kill "+arrApp.get(position).getPackageApp().toString(), Toast.LENGTH_SHORT).show();
				arrApp.remove(arrApp.get(position));
				
			}
		});
	}


	public void getApprun()
	{

		activityManager = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
		List<RunningTaskInfo> a=activityManager.getRunningTasks(Integer.MAX_VALUE);

		PackageManager pack=this.getPackageManager();

		for(int i=0;i<a.size();i++){

			String packageName = a.get(i).topActivity.getPackageName();

			Drawable d=null;
			String appName="";

			try {
				d=pack.getApplicationIcon(packageName);

				appName=(String)     pack.getApplicationLabel(pack.getApplicationInfo(packageName,PackageManager.GET_META_DATA));

			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}

			app  = new App2();
			app.setNameApp(appName);
			app.setIconApp(d);
			app.setPackageApp(packageName);
			arrApp.add(app);
			

		}
	}


}
